package analysis

import (
	"regexp"
	"strings"
)

var (
	targetType                    string
	targetConstructorLine         string
	targetConstructorSplittedLine []string

	//define possible targets
	targetWords = []string{
		"server",
		"service",
		"lb vserver",
		"lb monitor",
		"cs vserver",
		"vpn vserver",
		"ssl certKey",
	}

	//same for labels
	targetLabels = map[string]string{
		"server":      "Backend Server",
		"service":     "Backend Service",
		"lb vserver":  "Load Balancing Virtual Server",
		"lb monitor":  "Load Balancing Monitor",
		"cs vserver":  "Content Switching Virtual Server",
		"vpn vserver": "Access Gateway Virtual Server",
		"ssl certKey": "Certificate-Key Pair",
	}

	constructorLines = map[string]string{
		"server":      "add server ",
		"service":     "add service ",
		"lb vserver":  "add lb vserver ",
		"lb monitor":  "add lb monitor ",
		"cs vserver":  "add cs vserver ",
		"vpn vserver": "add vpn vserver ",
		"ssl certKey": "add ssl certKey ",
	}

	bindLines = map[string]string{
		"service":     "bind service ",
		"lb vserver":  "bind lb vserver ",
		"cs vserver":  "bind cs vserver ",
		"vpn vserver": "bind vpn vserver ",
		"ssl certKey": "link ssl certKey ",

		"ssl vserver": "bind ssl vserver ",
	}

	//http://www.golangprograms.com/regular-expression-to-extract-dns-host-name-or-ip-address-from-string.html
	ipRegexp     = regexp.MustCompile(`(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}`)
	typeRegexp   = regexp.MustCompile(`\sHTTP\s|\sSSL\s|\sTCP\s|\sUDP\s|\sMSSQL\s|\sMYSQL\s`)
	portRegexp   = regexp.MustCompile(`\s\d\s|\s\d\d\s|\s\d\d\d\s|\s\d\d\d\d\s|\s\d\d\d\d\d\s`)
	icaonlyRegxp = regexp.MustCompile(`\-icaOnly\s(ON|OFF)`)
)

//THIS NEEDS REVISION for strings.Split(line, " ")
//GetTargetType tries to understand target type
//a tipical costructor line starts with "add"
//followed by one of targetWords enties
//add lb vserver [Target]
func GetTargetType() []string {
	//iterate file lines
	for _, line := range NSConf {
		//check if it is a constructor line
		if strings.HasPrefix(line, "add") {
			//get a splitted version of the line
			splitLine := strings.Split(line, " ")
			//iterate line elements
			for _, t := range splitLine {
				//check if line match the given target
				if ParsedTarget.MatchString(t) {
					//remove 1st word of the line
					str := strings.Trim(line, "add ")
					//iterate possible targets
					for _, w := range targetWords {
						//check if trimmed line start with a possible targets
						if strings.HasPrefix(str, w) {
							//match found
							targetConstructorLine = line
							targetConstructorSplittedLine = splitLine
							targetType = w
							logger.AddRawLine(line)
							return splitLine
						}
					}
				}
			}
		}
	}

	return nil
}

//server has no child, so no analysis is needed
func _serverChildAnalysis() []string {
	return nil
}

//child relation for services is done in the constructor line
//since is the constructor line, we can assume that there will be only
//one bind for servie
//add service [serviceName] [serverName] [serviceType] [servicePort]
func _serviceChildAnalysis() (string, string, string, []string) {
	var monitors []string

	for _, line := range NSConf {
		if ParsedTarget.MatchString(line) {
			if strings.HasPrefix(line, bindLines[targetType]) {
				if r := strings.Index(line, "-monitorName"); r != -1 {
					ln := line[r:]
					ln = strings.TrimPrefix(ln, "-monitorName ")
					monitors = append(monitors, ln)

					logger.AddRawLine(line)
				}
			}
		}
	}

	return targetConstructorSplittedLine[3], targetConstructorSplittedLine[4], targetConstructorSplittedLine[5], monitors
}

//lbvserver constructor line
//add lb vserver [lbvserverName] [lbvserverType] [lbvserverAddress] [lbvserverPort]
//lbvserver child bind line
//bind lb vserver [lbvserverName] [serviceName]
func _lbvserverChildAnalysis() []string {
	var backends []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, bindLines[targetType]) {
			if ParsedTarget.MatchString(line) {
				//Don't strip line, instead remove everything but serviceName
				//remove bind keywords (bind lb vserver )
				ln := strings.TrimPrefix(line, bindLines[targetType])
				//remove extra spaces
				ln = strings.TrimSpace(ln)
				//remove target to get serviceName
				ln = strings.TrimPrefix(ln, Target)
				//remove extra spaces
				ln = strings.TrimSpace(ln)

				//store result
				backends = append(backends, ln)

				//add line to raw output
				logger.AddRawLine(line)
			}
		}
	}

	return backends
}

//lb monitor has no child
func _lbmonitorChildAnalysis() []string {
	return nil
}

//since cs binding is done with the -policyName and -priority flags, we need to strip those
//bind is done with bind ssl vserver for certificates and ecccurve
func _csvserverChildAnalysis() (string, []string) {
	var childs []string
	var certKey, ln string

	for _, line := range NSConf {
		if strings.HasPrefix(line, bindLines[targetType]) || strings.HasPrefix(line, bindLines["ssl vserver"]) {
			if ParsedTarget.MatchString(line) {
				//remove bind keywords (bind cs vserver )
				if strings.HasPrefix(line, bindLines[targetType]) {
					ln = strings.TrimPrefix(line, bindLines[targetType])
				} else if strings.HasPrefix(line, bindLines["ssl vserver"]) {
					ln = strings.TrimPrefix(line, bindLines["ssl vserver"])
				} else {
					continue
				}

				//remove extra spaces
				ln = strings.TrimSpace(ln)
				//remove target to get policyName
				ln = strings.TrimPrefix(ln, Target)

				if strings.HasPrefix(ln, " -certkeyName") {
					certKey = strings.TrimPrefix(ln, " -certkeyName ")
				} else if strings.HasPrefix(ln, " -policyName") {
					//remove -policyName flag
					ln = strings.TrimPrefix(ln, " -policyName ")

					//get index of "-prority" since we need to also trim that
					ln = ln[:strings.Index(ln, " -priority")]

					//store result
					childs = append(childs, ln)
				}

				//add line to raw output
				logger.AddRawLine(line)
			}
		}
	}
	return certKey, childs
}

func _vpnvserverChildAnalysis() ([]string, string, string, []string) {
	var staServer, childs []string
	var appController, certKey, ln string

	for _, line := range NSConf {
		if strings.HasPrefix(line, bindLines[targetType]) || strings.HasPrefix(line, bindLines["ssl vserver"]) {
			if ParsedTarget.MatchString(line) {
				//remove bind keywords (bind vpn vserver )
				//since binding can be done both by bind vpn and bind ssl we need to ensure trimming is done right
				if strings.HasPrefix(line, bindLines[targetType]) {
					ln = strings.TrimPrefix(line, bindLines[targetType])
				} else if strings.HasPrefix(line, bindLines["ssl vserver"]) {
					ln = strings.TrimPrefix(line, bindLines["ssl vserver"])
				} else {
					continue
				}

				//remove extra spaces
				ln = strings.TrimSpace(ln)
				//remove target to get policyName
				ln = strings.TrimPrefix(ln, Target)

				//retrive -staServer
				if strings.HasPrefix(ln, " -staServer") {
					staServer = append(staServer, strings.TrimPrefix(ln, " -staServer "))
					//retrive -appController
				} else if strings.HasPrefix(ln, " -appController") {
					appController = strings.TrimPrefix(ln, " -appController ")
					//retrive -certkeyName
				} else if strings.HasPrefix(ln, " -certkeyName") {
					certKey = strings.TrimPrefix(ln, " -certkeyName ")
					//retrive binding line
				} else if strings.HasPrefix(ln, " -policy ") {
					//remove -policy flag
					ln = strings.TrimPrefix(ln, " -policy ")

					//get index of "-prority" since we need to also trim that
					if strings.Contains(ln, "-priority") {
						ln = ln[:strings.Index(ln, " -priority")]
					}

					//store result
					childs = append(childs, ln)
				}

				//add line to raw output
				logger.AddRawLine(line)
			}
		}
	}

	return staServer, appController, certKey, childs
}

func _isicaonly() bool {
	line := icaonlyRegxp.FindString(targetConstructorLine)
	r := strings.Split(line, " ")
	if len(r) > 1 {
		if r[1] == "ON" {
			return true
		}
	}

	return false
}

func _sslcertkeyChildAnalysis() (string, string, []string) {
	var childs []string
	var cert, key string

	for _, line := range NSConf {
		if strings.HasPrefix(line, constructorLines[targetType]) {
			if ParsedTarget.MatchString(line) {
				ln := strings.TrimPrefix(line, constructorLines[targetType])
				ln = strings.TrimPrefix(ln, Target)
				ln = strings.TrimPrefix(ln, " -cert ")
				if r := strings.Index(ln, " -key"); r != -1 {
					cert = ln[:strings.Index(ln, " -key")]
				} else {
					cert = ln
				}
				if r := strings.Index(ln, " -passcrypt"); r != -1 {
					key = ln[strings.LastIndex(ln, "-key ")+5 : strings.Index(ln, " -passcrypt")]
				}
			}
		} else if strings.HasPrefix(line, bindLines[targetType]) {
			if ParsedTarget.MatchString(line) {
				ln := strings.TrimPrefix(line, bindLines[targetType])
				ln = strings.TrimPrefix(ln, Target)
				ln = strings.TrimSpace(ln)

				childs = append(childs, ln)
				logger.AddRawLine(line)
			}
		}
	}

	return cert, key, childs
}

////

//_FinalRound() iterate all NSConf lines to fine lines that matches given target for any reason
func _FinalRound() {
	for _, line := range NSConf {
		if ParsedTarget.MatchString(line) {
			logger.AddRawLine(line)
		}
	}
}
