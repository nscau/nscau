package analysis

import (
	"bufio"
	"flag"
	"os"
	"regexp"
	"strings"
)

//LoadFile load the given file and returns []byte
func LoadFile(path string) {
	//Print nscau usage if missing file path
	if path == "" {
		flag.Usage()
		os.Exit(0)
	}

	//open file from disk
	fh, err := os.Open(path)
	if err != nil {
		logger.Print("Error loading file: %s", err)
		os.Exit(1)
	}
	defer fh.Close()

	//get a new reader from opened file
	r := bufio.NewReader(fh)

	//empty container
	var tmpNSConf []string

	//parse file line per line
	for {
		line, err := r.ReadString('\n')
		tmpNSConf = append(tmpNSConf, line)
		if err != nil {
			break
		}
	}

	//parse file to remove \n at the end of every line
	for _, line := range tmpNSConf {
		ln := strings.TrimSuffix(line, "\n")
		NSConf = append(NSConf, ln)
	}
}

//ParseTarget returns the given target as a parsed regexp evaluator
func ParseTarget(target string) *regexp.Regexp {
	//convert target to a parsable regexp object
	rg := regexp.QuoteMeta(target)

	//add \s|$ to avoid matching object with same prefix
	rgxp := regexp.MustCompile(rg + `(\s|$)`)

	//return regexp.MustCompile(regexp.QuoteMeta(target))
	return rgxp
}

//PrintGeneralInfos prints geenral data goods for every analysis
func PrintGeneralInfos() {
	PrintNSConfFilePath()
	PrintNSConfLastEdit()

	logger.NewLine()
}
