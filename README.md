# NetScaler Configuration Analyzer Utility

- [Features](#Features)
- [Installation](#Installation)
- [Usage](#Usage)
- [Test](#Test)
- [Examples](#Examples)
- [Notes](#Notes)

NetScaler Configuration Analyzer Utility (nscau) is a standalone (and offline) tool written in golang based on work done by [Carl Stalhood](http://www.carlstalhood.com) on his [NetscalerExtractor](https://github.com/cstalhood/Get-ADCVServerConfig)

Goal of this tool is to extract relevant lines from a given ns.conf file

---

## Features

NSCAU supports the following Netscaler objects as target:

- Load Balancing Virtual Server
- Load Balancing Service
- Load Balancing Server
- Load Balancing Monitor
- Content Switching Virtual Server
- Netscaler Gateway Virtual Server
- CertKey Pair

NSCAU aims to supports the following Netscaler objects as target:

- Load Balancing ServiceGroup
- Content Switching Policy/Action
- URL Transformation Profile/Policy
- Responder Policy/Action
- Rewrite Policy/Action
- PatternSet
- StringMap
- Cipher Group
- Authentication Virtual Server
- GSLB Virtual Server
- TCP/HTTP/SSL/... Profile
- ...

---

## Installation

You need [golang](https://golang.org) in order to build the Utility

    me@localhost:# go version
    go version go1.11.2 darwin/amd64

After that, all you need to do is

    me@localhost:# go get -u gitlab.com/nscau/nscau

---

## Usage

    Usage of nscau:
        -bulk string
    	    Get a bulk analysis of the given type (ignored with -target)
        -nsconf string
    	    NetScaler configuration file to analyze (required)
        -raw
    	    Add raw output
        -target string
    	    Target to analyze

---

## Test

NSCAU has been tested with the following firmware version:

- 10.5
- 11.0
- 11.1
- 12.0
- 12.1

---

## Examples

    me@localhost:# nscau -nsconf /Users/me/Downloads/ns.conf

            ::::::::::::::::::::::::::::::::::::::::::::::
            ::             Welcome to NSCAU             ::
            :: NetScaler Configuration Analyzer Utility ::
            ::::::::::::::::::::::::::::::::::::::::::::::

    [*] Analyzing file                : /Users/me/Downloads/ns.conf
    [*] File saved on                 : Tue Nov 27 09:49:23 2018

    [*] NS Hostname                   : NSMPX01
    [*] NS Version                    : 11.1 56.15
    [*] NS IP                         : 10.1.1.1/255.255.255.0
    [*] NS Default Gateway            : 10.1.1.254
    [*] NS Vlan ID                    : 1
    [*] NS Features                   : WL SP LB CS SSL SSLVPN AAA REWRITE RESPONDER AppFlow CH
    [*] NS Modes                      : FR L3 MBF Edge USNIP PMTUD
    [*] NS Users                      : 37
    [*] NS Groups                     : 2
    [*] NS SNIP                       : 20
    [*] NS Defined VLANs              : 5
    [*] NS HA Nodes                   : 1
    [*] NS Servers                    : 117
    [*] NS Services                   : 440
    [*] NS ServiceGroups              : 12
    [*] NS CertKey Pair               : 10
    [*] NS LB VServers                : 192
    [*] NS CS VServers                : 14
    [*] NS AGW VServers               : 1
    [*] NS DNS NameServers            : 1
    [*] NS custom DNS Suffix          : 1
    [*] NS custom LB Monitors         : 38

---

## Notes

- I DO NOT work for/with Citrix
