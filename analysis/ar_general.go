package analysis

//PrintGlobalAnalysis print general data extracted from given nsconf file
func PrintGlobalAnalysis() {
	PrintNSHostName()
	PrintNSVersion()
	PrintNSTimeZone()
	PrintNSIP()
	PrintNSDefaultGateway()
	PrintHasPBR()
	PrintNSVlan()
	PrintNSFeatures()
	PrintNSModes()
	PrintNSUsers()
	PrintNSGroups()
	PrintNSSNIP()
	PrintNSVLANs()
	PrintNSHANodes()
	PrintNSServers()
	PrintNSServices()
	PrintNSServiceGroups()
	PrintNSSSLCertKeys()
	PrintNSLBVServers()
	PrintNSCSVServers()
	PrintNSAuthVServers()
	PrintNSAGWVServers()
	PrintNSDNSServers()
	PrintNSDNSSuffix()
	PrintNSLBMonitors()
}

//PrintNSConfFilePath prints nsconf file path
func PrintNSConfFilePath() {
	logger.Print("Analyzing file\t\t: %s", NSConfFilePath)
}

//PrintNSConfLastEdit prints time of write of the nsconf file
func PrintNSConfLastEdit() {
	logger.Print("File saved on\t\t: %s", GetNSConfLastEdit())
}

//PrintNSHostName prints appliance hostname
func PrintNSHostName() {
	logger.Print("NS Hostname\t\t\t: %s", GetNSHostName())
}

//PrintNSVersion prints appliance version
func PrintNSVersion() {
	logger.Print("NS Version\t\t\t: %s", GetNSVersion())
}

//PrintNSIP prints netscaler IP
func PrintNSIP() {
	logger.Print("NS IP\t\t\t: %s", GetNSIP())
}

//PrintNSVlan prints Netscaler default vlan
func PrintNSVlan() {
	logger.Print("NS Vlan ID\t\t\t: %s", GetNSVlan())
}

//PrintNSFeatures prints netscaler enabled features
func PrintNSFeatures() {
	logger.Print("NS Features\t\t\t: %s", GetNSFeatures())
}

//PrintNSModes prints netscaler enabled modes
func PrintNSModes() {
	logger.Print("NS Modes\t\t\t: %s", GetNSModes())
}

//PrintNSUsers prints netscaler custom users
func PrintNSUsers() {
	if len(GetNSUsers()) > 0 {
		logger.Print("NS Users\t\t\t: %d", len(GetNSUsers()))
	}
}

//PrintNSGroups prints netscaler custom groups
func PrintNSGroups() {
	if len(GetNSGroups()) > 0 {
		logger.Print("NS Groups\t\t\t: %d", len(GetNSGroups()))
	}
}

//PrintNSSNIP prints netscaler subnet ip
func PrintNSSNIP() {
	if len(GetNSSNIP()) > 0 {
		logger.Print("NS SNIP\t\t\t: %d", len(GetNSSNIP()))
	}
}

//PrintNSVLANs prints netscaler defined vlans
func PrintNSVLANs() {
	if len(GetNSVLANs()) > 0 {
		logger.Print("NS Defined VLANs\t\t: %d", len(GetNSVLANs()))
	}
}

//PrintNSHANodes prints netscaler HA nodes
func PrintNSHANodes() {
	if len(GetNSHANodes()) > 0 {
		logger.Print("NS HA Nodes\t\t\t: %d", len(GetNSHANodes()))
	}
}

//PrintNSServers prints netscaler defined servers
func PrintNSServers() {
	if len(GetNSServers()) > 0 {
		logger.Print("NS Servers\t\t\t: %d", len(GetNSServers()))
	}
}

//PrintNSServices prints netscaler defined services
func PrintNSServices() {
	if len(GetNSServices()) > 0 {
		logger.Print("NS Services\t\t\t: %d", len(GetNSServices()))
	}
}

//PrintNSServiceGroups prints netscaler defined groups
func PrintNSServiceGroups() {
	if len(GetNSServiceGroups()) > 0 {
		logger.Print("NS ServiceGroups\t\t: %d", len(GetNSServiceGroups()))
	}
}

//PrintNSSSLCertKeys prints netscaler defined CertKey pair
func PrintNSSSLCertKeys() {
	if len(GetNSSSLCertKeys()) > 0 {
		logger.Print("NS CertKey Pair\t\t: %d", len(GetNSSSLCertKeys()))
	}
}

//PrintNSLBVServers prints netscaler load balancing virtual servers
func PrintNSLBVServers() {
	if len(GetNSLBVServers()) > 0 {
		logger.Print("NS LB VServers\t\t: %d", len(GetNSLBVServers()))
	}
}

//PrintNSCSVServers prints netscaler content switching virtual servers
func PrintNSCSVServers() {
	if len(GetNSCSVServers()) > 0 {
		logger.Print("NS CS VServers\t\t: %d", len(GetNSCSVServers()))
	}
}

//PrintNSAuthVServers prints netscaler auth virtual servers
func PrintNSAuthVServers() {
	if len(GetNSAuthVServers()) > 0 {
		logger.Print("NS Auth VServers\t\t: %d", len(GetNSAuthVServers()))
	}
}

//PrintNSAGWVServers prints netscaler access gateway virtual servers
func PrintNSAGWVServers() {
	if len(GetNSAGWVServers()) > 0 {
		logger.Print("NS AGW VServers\t\t: %d", len(GetNSAGWVServers()))
	}
}

//PrintNSDNSServers prints netscaler Domain Name Servers
func PrintNSDNSServers() {
	if len(GetNSDNSNameServers()) > 0 {
		logger.Print("NS DNS NameServers\t\t: %d", len(GetNSDNSNameServers()))
	}
}

//PrintNSDefaultGateway prints netsacler default gateway
func PrintNSDefaultGateway() {
	logger.Print("NS Default Gateway\t\t: %s", GetNSDefaultGateway())
}

//PrintNSDNSSuffix prints netscaler defined dns suffix
func PrintNSDNSSuffix() {
	if len(GetNSDNSSuffix()) > 0 {
		logger.Print("NS custom DNS Suffix\t: %d", len(GetNSDNSSuffix()))
	}
}

//PrintNSLBMonitors prints netscaler defined monitors
func PrintNSLBMonitors() {
	if len(GetNSLBMonitors()) > 0 {
		logger.Print("NS custom LB Monitors\t: %d", len(GetNSLBMonitors()))
	}
}

//PrintHasPBR prints true if PBR is active
func PrintHasPBR() {
	if HasPBR() {
		logger.Print("NS BPR is Active\t\t: %s", "True")
	}
}

//PrintNSTimeZone prints netscaler timezone
func PrintNSTimeZone() {
	if GetNSTimeZone() != "" {
		logger.Print("NS TimeZone\t\t\t: %s", GetNSTimeZone())
	}
}
