package analysis

import (
	"flag"
	"regexp"
)

var (
	//NSConfFilePath holds NetScaler configuration file path
	NSConfFilePath string

	//NSConf holds parsed (per-line) NetScaler configuration file
	NSConf []string

	//Target holds target to analyze
	Target string

	//ParsedTarget holds the regexp version of the target
	ParsedTarget *regexp.Regexp

	//RawOutput if true additional output as raw Netscaler configuration will be printed
	RawOutput bool

	//Bulk holds type for bulk analysis
	Bulk string
)

//Parse parse flags for options
func Parse() {
	flag.StringVar(&NSConfFilePath, "nsconf", "", "NetScaler configuration file to analyze (required)")
	flag.StringVar(&Target, "target", "", "Target to analyze")
	flag.BoolVar(&RawOutput, "raw", false, "Add raw output")
	flag.StringVar(&Bulk, "bulk", "", "Get a bulk analysis of the given type (ignored with -target)")

	flag.Parse()
}
