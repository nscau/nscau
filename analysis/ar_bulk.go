package analysis

import "os"

//PrintBulkAnalysis prints results from bulk analysis of given bulk target
func PrintBulkAnalysis() {
	logger.Print("Bulk Analysis Target\t: %s", Bulk)

	if res := IsBulkTarget(Bulk); res != true {
		logger.PrintError("Cannot analyze bulk target\t: %s", Bulk)
		logger.NewLine()
		logger.Print("Available Bulk targets: ")
		for _, t := range bulks {
			logger.PrintError("\t %s", t)
		}
		os.Exit(1)
	}

	bulks := _BulkAnalysis()

	switch Bulk {
	case "server":
		logger.Print("Backend Servers\t\t: %d", len(bulks))
	case "service":
		logger.Print("Backend Services\t\t: %d", len(bulks))
	case "lb vserver":
		logger.Print("Load Balancing VServers\t: %d", len(bulks))
	case "lb monitor":
		logger.Print("Load Balancing Monitors\t: %d", len(bulks))
	case "cs vserver":
		logger.Print("Content Switching VServer\t: %d", len(bulks))
	case "vpn vserver":
		logger.Print("VPN VServers\t\t: %d", len(bulks))
	case "ssl certKey":
		logger.Print("SSL CertKey Pair\t\t: %d", len(bulks))
	}
}
