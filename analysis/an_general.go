package analysis

import (
	"regexp"
	"strings"
)

var (
	nsHostNameString     = "set ns hostName "
	nsFeaturesString     = "enable ns feature "
	nsModeString         = "enable ns mode "
	nsUserString         = "add system user "
	nsGroupString        = "add system group "
	nsConfigString       = "set ns config "
	nsSNIPString         = "add ns ip "
	nsVLANString         = "add vlan "
	nsHANodeString       = "add HA node "
	nsServerString       = "add server "
	nsServiceString      = "add service "
	nsServiceGroupString = "add serviceGroup "
	nsCertKeyString      = "add ssl certKey "
	nsLBVServerString    = "add lb vserver "
	nsCSVServerString    = "add cs vserver "
	nsAuthVServerString  = "add authentication vserver "
	nsAGWVServerString   = "add vpn vserver "
	nsDNSServerString    = "add dns nameServer "
	nsDNSSuffixString    = "add dns suffix "
	nsLBMonitorString    = "add lb monitor "
	nsTimeZoneString     = "set ns param -timezone "
	nsDefaultRouteString = "add route 0.0.0.0 0.0.0.0 "
	nsPBRString          = "add ns pbr "
)

//GetNSConfLastEdit returns date on which the file has been saved
func GetNSConfLastEdit() string {
	editLine := NSConf[1]

	r := strings.Split(editLine, ",")

	date := r[len(r)-1]

	return strings.TrimSpace(date)
}

//GetNSHostName returns NS hostname
func GetNSHostName() string {
	hostnameLine := ""

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsHostNameString) {
			hostnameLine = line
		}
	}

	hostname := strings.TrimPrefix(hostnameLine, nsHostNameString)
	return hostname
}

//GetNSVersion extracts NetScaler version from configuration file
func GetNSVersion() string {
	versionLine := NSConf[0]

	r := regexp.MustCompile("\\d(\\d)?\\.\\d(\\d)?")
	match := r.FindAllString(versionLine, -1)

	//this does not break because is no user input
	return strings.Join(match, " ")
}

//GetNSIP returns NS IP
func GetNSIP() string {
	var nsConfigLines []string
	var ip, mask string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsConfigString) {
			nsConfigLines = append(nsConfigLines, line)
		}
	}

	for _, line := range nsConfigLines {
		//This does not break because it a system line
		//thus no spaces should be used for object names
		splitLine := strings.Split(line, " ")
		for i, ln := range splitLine {
			if ln == "-IPAddress" {
				ip = splitLine[i+1]
			} else if ln == "-netmask" {
				mask = splitLine[i+1]
			}
		}
	}

	return ip + "/" + mask
}

//GetNSVlan returns NS native Vlan
func GetNSVlan() string {
	var nsConfigLines []string
	var nsvlan string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsConfigString) {
			nsConfigLines = append(nsConfigLines, line)
		}
	}

	for _, line := range nsConfigLines {
		//This does not break because it a system line
		//thus no spaces should be used for object names
		splitLine := strings.Split(line, " ")
		for i, ln := range splitLine {
			if ln == "-nsvlan" {
				nsvlan = splitLine[i+1]
			}
		}
	}

	if nsvlan == "" {
		return "1"
	}

	return nsvlan
}

//GetNSFeatures returns string with enabled features
func GetNSFeatures() string {
	featuresLine := ""

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsFeaturesString) {
			featuresLine = line
		}
	}

	if featuresLine == "" {
		return featuresLine
	}

	features := strings.Trim(featuresLine, nsFeaturesString)
	return features
}

//GetNSModes returns string with enabled features
func GetNSModes() string {
	modesLine := ""

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsModeString) {
			modesLine = line
		}
	}

	if modesLine == "" {
		return modesLine
	}

	modes := strings.Trim(modesLine, nsModeString)
	return modes
}

//GetNSUsers returns a slice of system users
func GetNSUsers() []string {
	var userLines []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsUserString) {
			userLines = append(userLines, line)
		}
	}

	return userLines
}

//GetNSGroups returns a slice of system users
func GetNSGroups() []string {
	var groupLines []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsGroupString) {
			groupLines = append(groupLines, line)
		}
	}

	return groupLines
}

//GetNSSNIP returns a list of NetScaler SubNetIP
func GetNSSNIP() []string {
	var snipLines []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsSNIPString) {
			snipLines = append(snipLines, line)
		}
	}

	return snipLines
}

//GetNSVLANs returns a list of defined vlan inside NetScaler
func GetNSVLANs() []string {
	var vlanLines []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsVLANString) {
			vlanLines = append(vlanLines, line)
		}
	}

	return vlanLines
}

//GetNSHANodes returns a list of HA nodes
func GetNSHANodes() []string {
	var haNodeLines []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsHANodeString) {
			haNodeLines = append(haNodeLines, line)
		}
	}

	return haNodeLines
}

//GetNSServers returns a list of physical server defined in NetScaler
func GetNSServers() []string {
	var serverLines []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsServerString) {
			serverLines = append(serverLines, line)
		}
	}

	return serverLines
}

//GetNSServices returns a list of services defined in NetScaler
func GetNSServices() []string {
	var serviceLines []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsServiceString) {
			serviceLines = append(serviceLines, line)
		}
	}

	return serviceLines
}

//GetNSServiceGroups returns a list of serviceGroup defined in NetScaler
func GetNSServiceGroups() []string {
	var sgroupLines []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsServiceGroupString) {
			sgroupLines = append(sgroupLines, line)
		}
	}

	return sgroupLines
}

//GetNSSSLCertKeys returns a list of certkey pair defined in NetScaler
func GetNSSSLCertKeys() []string {
	var certKeyLines []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsCertKeyString) {
			certKeyLines = append(certKeyLines, line)
		}
	}

	return certKeyLines
}

//GetNSLBVServers returns a list of LB Vserver defined in NetScaler
func GetNSLBVServers() []string {
	var lbvserverLines []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsLBVServerString) {
			lbvserverLines = append(lbvserverLines, line)
		}
	}

	return lbvserverLines
}

//GetNSCSVServers returns a list of CS Vserver defined in NetScaler
func GetNSCSVServers() []string {
	var csvserverLines []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsCSVServerString) {
			csvserverLines = append(csvserverLines, line)
		}
	}

	return csvserverLines
}

//GetNSAuthVServers returns a list of Auth Vserver defined in NetScaler
func GetNSAuthVServers() []string {
	var authvserverLines []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsAuthVServerString) {
			authvserverLines = append(authvserverLines, line)
		}
	}

	return authvserverLines
}

//GetNSAGWVServers returns a list of AGW VServer defined in NetScaler
func GetNSAGWVServers() []string {
	var agwvserverLines []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsAGWVServerString) {
			agwvserverLines = append(agwvserverLines, line)
		}

	}

	return agwvserverLines
}

//GetNSDNSNameServers returns a list of DNS NameServer defined in NetScaler
func GetNSDNSNameServers() []string {
	var dnsserverLines []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsDNSServerString) {
			dnsserverLines = append(dnsserverLines, line)
		}
	}

	return dnsserverLines
}

//GetNSDefaultRoute returns the NetScaler default route
func GetNSDefaultRoute() string {
	for _, line := range NSConf {
		if strings.HasPrefix(line, nsDefaultRouteString) {
			return line
		}
	}

	return ""
}

//GetNSDefaultGateway returns the NetScaler default gateway
func GetNSDefaultGateway() string {
	gwLine := GetNSDefaultRoute()

	//This does not break because it a system line
	tm := strings.Split(gwLine, " ")

	return tm[len(tm)-1]
}

//GetNSDNSSuffix returns a list of custom DNS Suffix defined in NetScaler
func GetNSDNSSuffix() []string {
	var dnssuffLines []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsDNSSuffixString) {
			dnssuffLines = append(dnssuffLines, line)
		}
	}

	return dnssuffLines
}

//GetNSLBMonitors returns a list of custom LB Monitor defined in NetScaler
func GetNSLBMonitors() []string {
	var lbmonLines []string

	for _, line := range NSConf {
		if strings.HasPrefix(line, nsLBMonitorString) {
			lbmonLines = append(lbmonLines, line)
		}
	}

	return lbmonLines
}

//HasPBR returns true if NetScaler has at least one pbr line
//That means PBR is active
func HasPBR() bool {
	for _, line := range NSConf {
		if strings.HasPrefix(line, nsPBRString) {
			return true
		}
	}

	return false
}

//GetNSTimeZone returns NetScaler timezone
func GetNSTimeZone() string {
	for _, line := range NSConf {
		if strings.HasPrefix(line, nsTimeZoneString) {
			ln := strings.TrimPrefix(line, nsTimeZoneString)
			trimln := ln[1 : len(ln)-1]
			tz := strings.Split(trimln, "-")
			return tz[len(tz)-1]
		}
	}

	return ""
}
