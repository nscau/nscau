package analysis

import (
	"os"
	"strings"
)

//PrintTargetAnalysis execute analysis on given target
func PrintTargetAnalysis() {
	logger.Print("Target\t\t\t: %s", Target)
	PrintTargetType()
	PrintTargetChildAnalysis()
}

//PrintTargetType print the output of GetType
func PrintTargetType() {
	tyles := GetTargetType()
	if tyles == nil {
		logger.NewLine()
		logger.PrintError("Failed to calculate type for target\t: %s", Target)
		os.Exit(1)
	} else {
		logger.Print("Target Type\t\t\t: %s", targetLabels[targetType])
	}
}

//PrintTargetChildAnalysis prints target childs
func PrintTargetChildAnalysis() {
	logger.NewLine()

	switch targetType {
	case targetWords[0]: // server
		_ = _serverChildAnalysis()
		ip := ipRegexp.FindString(targetConstructorLine)
		if ip != "" {
			logger.Print("Target IP Address\t\t: %s", ip)
		}
	case targetWords[1]: // service
		childName, childType, childPort, monitors := _serviceChildAnalysis()
		logger.Print("Target Backend Server\t: %s", childName)
		logger.Print("Target Backend Type\t\t: %s", childType)
		logger.Print("Target Backend Port\t\t: %s", childPort)
		for i, v := range monitors {
			if i <= 9 {
				logger.Print("Target Backend Monitor 0%d\t: %s", i, v)
			} else {
				logger.Print("Target Backend Monitor %d\t: %s", i, v)
			}
		}
	case targetWords[2]: // lb vserver
		ip := ipRegexp.FindString(targetConstructorLine)
		if ip != "" {
			logger.Print("Target IP Address\t\t: %s", ip)
		}
		tp := typeRegexp.FindString(targetConstructorLine)
		if tp != "" {
			logger.Print("Target Type\t\t\t: %s", strings.TrimSpace(tp))
		}
		pt := portRegexp.FindString(targetConstructorLine)
		if pt != "" {
			logger.Print("Target Port\t\t\t: %s", strings.TrimSpace(pt))
		}

		logger.NewLine()

		backends := _lbvserverChildAnalysis()
		for i, v := range backends {
			logger.Print("Target Backend Service %d\t: %s", i, v)
		}
	case targetWords[3]: // lb monitor
		_ = _lbmonitorChildAnalysis()
	case targetWords[4]: // cs vserver
		ip := ipRegexp.FindString(targetConstructorLine)
		if ip != "" {
			logger.Print("Target IP Address\t\t: %s", ip)
		}
		tp := typeRegexp.FindString(targetConstructorLine)
		if tp != "" {
			logger.Print("Target Type\t\t\t: %s", strings.TrimSpace(tp))
		}
		pt := portRegexp.FindString(targetConstructorLine)
		if pt != "" {
			logger.Print("Target Port\t\t\t: %s", strings.TrimSpace(pt))
		}

		certKey, childs := _csvserverChildAnalysis()
		if certKey != "" {
			logger.Print("Target CertKey Pair\t\t: %s", certKey)
		}

		logger.NewLine()

		for i, v := range childs {
			if i <= 9 {
				logger.Print("Target CS Policy 0%d\t\t: %s", i, v)
			} else {
				logger.Print("Target CS Policy %d\t\t: %s", i, v)
			}

		}
	case targetWords[5]: // vpn vserver
		staServer, appController, certKey, childs := _vpnvserverChildAnalysis()
		ip := ipRegexp.FindString(targetConstructorLine)
		if ip != "" {
			logger.Print("Target IP Address\t\t: %s", ip)
		}
		tp := typeRegexp.FindString(targetConstructorLine)
		if tp != "" {
			logger.Print("Target Type\t\t\t: %s", strings.TrimSpace(tp))
		}
		pt := portRegexp.FindString(targetConstructorLine)
		if pt != "" {
			logger.Print("Target Port\t\t\t: %s", strings.TrimSpace(pt))
		}

		if _isicaonly() {
			logger.Print("Target Is ICA Only\t\t: Yes")
		} else {
			logger.Print("Target Is ICA Only\t\t: No")
		}

		for k, v := range staServer {
			if k <= 9 {
				logger.Print("Target STA Server 0%d\t: %s", k, v)
			} else {
				logger.Print("Target STA Server %d\t: %s", k, v)
			}
		}

		if appController != "" {
			logger.Print("Target APP Controller\t: %s", appController)
		}

		if certKey != "" {
			logger.Print("Target CertKey Pair\t\t: %s", certKey)
		}

		logger.NewLine()

		for i, v := range childs {
			if i <= 9 {
				logger.Print("Target VPN Policy 0%d\t: %s", i, v)
			} else {
				logger.Print("Target VPN Policy %d\t: %s", i, v)
			}

		}
	case targetWords[6]: // ssl certKey
		cert, key, childs := _sslcertkeyChildAnalysis()
		logger.Print("Target Certificate\t\t: %s", cert)
		if key != "" {
			logger.Print("Target Key\t\t\t: %s", key)
		}

		logger.NewLine()

		for _, v := range childs {
			logger.Print("Target Link\t\t\t: %s", v)
		}
	}

	_FinalRound()
}
