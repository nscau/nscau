package analysis

import (
	"strings"
)

//Bulk analysis aims to provide some flags for bulk analysis of a type
//Like ./nscau -nsconf .... -bulk "lb vserver"

var (
	bulks = []string{
		"server",
		"service",
		"lb vserver",
		"cs vserver",
		"vpn vserver",
		"ssl certKey",
	}
)

//IsBulkTarget returns true if given target is in the bulk target list
func IsBulkTarget(t string) bool {
	for _, v := range bulks {
		if res := strings.Compare(t, v); res == 0 {
			return true
		}
	}

	return false
}

func _BulkAnalysis() []string {
	var bulks []string

	constructorPrefix := constructorLines[Bulk]

	//iterate NSConf
	for _, line := range NSConf {
		//if string starts with constructor line for given bulk target
		if strings.HasPrefix(line, constructorPrefix) {
			//save line
			bulks = append(bulks, line)

			//add line to raw output
			logger.AddRawLine(line)
		}
	}

	return bulks
}
