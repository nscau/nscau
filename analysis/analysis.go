package analysis

func Analysis() {
	//Parse flags
	Parse()

	//Init logger and print intro
	NewLogger()
	logger.Intro()

	//Try to load given configuration file
	LoadFile(NSConfFilePath)

	//Print general infos from loaded nsconf
	//good data for every analysis
	PrintGeneralInfos()

	//If there's no target and no Bulk has been requested
	//do a GlobalAnalysis and exit
	if Target == "" && Bulk == "" {
		PrintGlobalAnalysis()
		return
	}

	//Do Bulk and exit
	//If -target, ignore -bulk
	if Bulk != "" && Target == "" {
		//do bulk analysis on given type
		PrintBulkAnalysis()
	}

	if Target != "" {
		//Parse and validate given target
		ParsedTarget = ParseTarget(Target)
	}

	if Bulk == "" {
		//Start target analysis
		PrintTargetAnalysis()
	}

	//Print RAW Output if requested with -raw
	//If -bulk, ignore -raw
	if RawOutput == true /*&& Bulk == ""*/ {
		PrintRawOutput()
	}

	return
}
