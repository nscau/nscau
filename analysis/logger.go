package analysis

import (
	"fmt"
	"strings"
)

var (
	prompt     = "[*]"
	dashPrompt = "[-]"
	plusPrompt = "[+]"

	logger *Logger
)

//Logger holds structure to print data
type Logger struct {
	Prompt       string
	DashPrompt   string
	PlusPrompt   string
	PrintedLines []string
	RawOutput    []string
}

//NewLogger returns a new Logger instance
func NewLogger() {
	l := &Logger{
		Prompt:       prompt,
		DashPrompt:   dashPrompt,
		PlusPrompt:   plusPrompt,
		PrintedLines: make([]string, 0),
		RawOutput:    make([]string, 0),
	}

	logger = l
}

//NewLine prints an empty line
func (l *Logger) NewLine() {
	fmt.Printf("\n")
}

//Print prints given data
func (l *Logger) Print(format string, args ...interface{}) {
	payload := fmt.Sprintf(format, args...)
	fmt.Printf(l.Prompt + " " + payload + "\n")
}

//PrintError prints given data with dashed prompt [-]
func (l *Logger) PrintError(format string, args ...interface{}) {
	payload := fmt.Sprintf(format, args...)
	fmt.Printf(l.DashPrompt + " " + payload + "\n")
}

//PrintRaw prints given data without prompt
func (l *Logger) PrintRaw(format string, args ...interface{}) {
	payload := fmt.Sprintf(format, args...)
	fmt.Printf(payload + "\n")
}

//Intro prints title and some infos
func (l *Logger) Intro() {
	logger.NewLine()
	logger.PrintRaw("\t::::::::::::::::::::::::::::::::::::::::::::::")
	logger.PrintRaw("\t::             Welcome to NSCAU             ::")
	logger.PrintRaw("\t:: NetScaler Configuration Analyzer Utility ::")
	logger.PrintRaw("\t::::::::::::::::::::::::::::::::::::::::::::::")
	logger.NewLine()
}

//AddRawLine adds the given RAW line to RawOutput slice
func (l *Logger) AddRawLine(line string) {
	//Add raw line only if it is not saved yet
	for _, ln := range l.RawOutput {
		if res := strings.Compare(ln, line); res == 0 {
			return
		}
	}

	l.RawOutput = append(l.RawOutput, line)
}

//PrintRawOutput prints saved output in Raw netscaler format
func PrintRawOutput() {
	logger.NewLine()
	logger.Print("Start RAW Output")

	for _, line := range logger.RawOutput {
		fmt.Println(line)
	}
}
